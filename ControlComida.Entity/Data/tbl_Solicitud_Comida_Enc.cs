//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ControlComida.Entity.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Solicitud_Comida_Enc
    {
        public tbl_Solicitud_Comida_Enc()
        {
            this.tbl_Solicitud_Comida_Det = new HashSet<tbl_Solicitud_Comida_Det>();
        }
    
        public int No_Solicitud { get; set; }
        public int Tipo_Comida { get; set; }
        public int AreaSolicitud { get; set; }
        public int Comedor { get; set; }
        public string Observaciones { get; set; }
        public string Usuario_Agrega { get; set; }
        public string Usuario_Autoriza { get; set; }
        public string Usuario_Verifica { get; set; }
        public System.DateTime Fecha_Ingreso { get; set; }
        public System.DateTime Fecha_Solicitud { get; set; }
    
        public virtual AreaSolicitante AreaSolicitante { get; set; }
        public virtual Comedor Comedor1 { get; set; }
        public virtual ICollection<tbl_Solicitud_Comida_Det> tbl_Solicitud_Comida_Det { get; set; }
        public virtual TipoComida TipoComida { get; set; }
    }
}
