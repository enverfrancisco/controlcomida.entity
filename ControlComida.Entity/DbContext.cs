﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlComida.Entity.Data;

namespace ControlComida.Entity
{
    public class DbContext : ControlComidaEntities, IDisposable
    {
        public DbContext()
        {
            base.Configuration.LazyLoadingEnabled = false;
        }

        public void Dispose()
        {
            base.Dispose();
        }        
    }

    public class CrearSolContext : ControlComidaEntities, IDisposable
    {
        public void CreaSolicitud(int vNoSolicitud, int vTipoComida, int vAreaSolicitud, int vComedor, string vObservaciones, string vUsuarioAgrega, string vUsuarioAutoriza, string vUsuarioVerifica, DateTime vFechaIngreso, DateTime vFechaSolicitud, int vCodigoEmpleado, int vHorarioTrabajo)
        {
            Crear_Solicitud(vNoSolicitud, vTipoComida, vAreaSolicitud, vComedor, vObservaciones, vUsuarioAgrega, vUsuarioAutoriza, vUsuarioVerifica, vFechaIngreso, vFechaSolicitud, vCodigoEmpleado, vHorarioTrabajo);
        }
    }    
}
